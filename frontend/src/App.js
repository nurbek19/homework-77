import React, {Component} from 'react';
import {connect} from 'react-redux';

import './App.css';
import Form from "./components/Form/Form";
import {createAdd, fetchAds} from "./store/action";
import Add from "./components/Add/Add";

class App extends Component {
    state = {
        author: '',
        message: '',
        image: ''
    };

    componentDidMount() {
        this.props.onFetchAds();
    }

    changeValue = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    createProduct = event => {
        event.preventDefault();

        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key])
        });

        this.props.onAddCreated(formData).then(() => {
            this.props.onFetchAds();
            this.setState({
                author: '',
                message: ''
            });
        });
    };

    render() {

        return (
            <div className="App">

                {this.props.ads.map(add => (
                    <Add
                        key={add.id}
                        img={add.image}
                        author={add.author}
                        message={add.message}
                    />
                ))}

                <Form
                    author={this.state.author}
                    message={this.state.message}
                    changeValue={this.changeValue}
                    fileChange={this.fileChangeHandler}
                    save={this.createProduct}
                />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        ads: state.ads,
        loading: state.loading
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchAds: () => dispatch(fetchAds()),
        onAddCreated: addData => dispatch(createAdd(addData))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
