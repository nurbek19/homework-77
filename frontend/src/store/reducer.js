import {CREATE_ADD_REQUEST, CREATE_ADD_SUCCESS, FETCH_ADD_SUCCESS} from "./action";

const initialState = {
    ads: [],
    loading: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ADD_SUCCESS:
            return {...state, ads: action.ads};
        case CREATE_ADD_REQUEST:
            return {...state, loading: true};
        case CREATE_ADD_SUCCESS:
            return {...state, loading: false};
        default:
            return state;
    }
};

export default reducer;