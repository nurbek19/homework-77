import axios from '../axios-api';

export const FETCH_ADD_SUCCESS = 'FETCH_ADD_SUCCESS';
export const CREATE_ADD_REQUEST = 'CREATE_PRODUCT_REQUEST';
export const CREATE_ADD_SUCCESS = 'CREATE_PRODUCT_SUCCESS';


export const fetchAddSuccess = ads => {
    return {type: FETCH_ADD_SUCCESS, ads}
};

export const fetchAds = () => {
    return dispatch => {
        return axios.get('/ads').then(response => {
            dispatch(fetchAddSuccess(response.data));
        })
    }
};

export const createAddRequest = () => {
    return {type: CREATE_ADD_REQUEST}
};

export const createAddSuccess = () => {
    return {type: CREATE_ADD_SUCCESS}
};

export const createAdd = addData => {
    return dispatch => {
        dispatch(createAddRequest());

        return axios.post('/ads', addData).then(() => {
            dispatch(createAddSuccess());
        })
    }
};