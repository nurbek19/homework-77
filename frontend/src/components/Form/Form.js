import React from 'react';
import './Form.css';

const Form = props => {
    return (
        <form action="#" className="add-form" onSubmit={props.save}>
            <input type="text" placeholder="Author"
                   name="author"
                   onChange={props.changeValue}
                   value={props.author}
            />

            <textarea name="message" cols="30" rows="10"
                      placeholder="Message"
                      onChange={props.changeValue}
                      value={props.message}
                      required
            />

            <input type="file" placeholder="Image"
                   name="image"
                   onChange={props.fileChange}
            />

            <button type="submit">Save</button>

        </form>
    )
};

export default Form;