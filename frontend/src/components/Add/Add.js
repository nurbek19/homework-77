import React from 'react';
import './Add.css';

const Add = props => {
    return(
        <div className="add">
            {props.img ? <img src={props.img} alt="add"/> : null}


            <div className="add-description">
                <h4>{props.message}</h4>
                <p>{props.author}</p>
            </div>
        </div>
    )
};

export default Add;