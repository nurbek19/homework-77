const express = require('express');
const multer = require('multer');
const router = express.Router();
const path = require('path');
const nanoid = require('nanoid');

const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = db => {

    router.get('/', (req, res) => {
        res.send(db.getData());
    });

    router.post('/', upload.single('image'), (req, res) => {

        if (req.body.message === '') {
            res.send({error: 'Message field should not be empty!'});
        } else {
            const product = req.body;

            if (req.file) {
                product.image = 'http://localhost:8000/uploads/' + req.file.filename;
            } else {
                product.image = null;
            }

            db.addItem(product).then(result => {
                res.send(result);
            });
        }
    });

    return router;
};

module.exports = createRouter;